﻿using GoogleAuthTest2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GoogleAuthTest2.Controllers
{
    public class GoogleSignInController : ApiController
    {
        [Route("~/signin-google")]
        public HttpResponseMessage Post(IdTokenModel param)
        {
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("signed in with google" + param.IdToken)
            };
        }

        public HttpResponseMessage GetToken(string idtoken)
        {
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("signed in with google" + idtoken)
            };
        }

    }
}
